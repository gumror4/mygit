import re
import urllib.request
import urllib.parse
import json

from mysqlite import DBcontroller
from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json

from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = 'xoxb-691650369815-683495033825-OxHYWF0FGOPP7C3uNuTw2ZaK'
SLACK_SIGNING_SECRET = '700902c56a27554aa9f51072d81bc250'

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.

slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

def crawl_dictionary(search, user):
    # DB 관리 객체
    db_master = DBcontroller()

    if "검색" in search:
        searchword = search.split('검색')[1][1:]

        url = "https://dict.naver.com/search.nhn?dicQuery=" + searchword + "&query=student&target=dic&ie=utf8&query_utf=&isOnlyViewEE="
        url2 = "https://endic.naver.com/search.nhn?sLn=kr&isOnlyViewEE=N&query=" + searchword

        req = urllib.request.Request(url)
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")

        outprint = ''
        wordarea = []
        wordpro = []
        tempstr = []
        resulttxt = ""
        pro = []

        # 단어라인 정보묶음

        try:
            word = soup.find("div", class_="en_dic_section").find("dt").find("a").get_text().strip()
        except:
            return "단어의 철자가 정확한지 확인해 보세요"

        # word가 오타일시 여기로 넘어오지 못함
        for i in soup.find("div", class_="en_dic_section").find("dt").find_all("span", class_="fnt_k10"):
            wordarea.append(i.get_text().strip())
        for i in soup.find("div", class_="en_dic_section").find("dt").find_all("span", class_="fnt_e25"):
            wordpro.append(i.get_text().strip())
        for k in range(len(wordpro)):
            tempstr.append(wordarea[k] + " : ")
            tempstr.append(wordpro[k] + " ")

        mean = str(soup.find("div", class_="en_dic_section").find("dd")).replace("<dd>", "").replace('</dd>',
                                                                                                     '').strip().replace('<br/>', '\n')
        resulttxt = ''.join(tempstr)

        # test2
        sourcecode = urllib.request.urlopen(url2).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        finded = soup.find("dl", class_="list_e2").find("dd").find("div", class_="align_right").find("p", class_="bg").\
            find("span", class_="fnt_e07").get_text()

        if searchword == word:
            outprint = "*" + word + " - " + resulttxt + "*\n" + mean + "\nExample : " + finded
            db_master.insert((user, word, mean))
        else:
            outprint = "일치하는 단어를 찾을 수 없습니다."
        return outprint

    elif "단어장" in search:
        outprint = "<@" + user + ">" + "님의 단어장입니다." + '\n'

        num = 1
        if len(db_master.selectAll(user)) > 0:
            for word in db_master.selectAll(user):
                outprint += "*(" + str(num) + ")  " + word[1] + "*" + '\n' + word[2] + '\n\n\n'
                num+=1
        else:
            outprint = "단어장에 단어가 없습니다."
        return outprint

    elif "문제" in search:
        testWord = db_master.random(user)  # 단어장에서 가져올 데이터(랜덤)

        if testWord:  # 단어장 리스트에 단어(튜플형)이 존재한다면
            # 퀴즈 삽입
            db_master.insert_quiz((user, testWord[1]))
        else:
            return "저장된 단어가 없습니다."

        outprint = "<@" + user + ">" + "님 다음 뜻에 맞는 영어 단어를 입력하세요.\n  " + testWord[2]
        outprint += "\n<@dic 정답 sky(정답단어) 형태로 입력하세요. >\n"

        return outprint

    elif "정답" in search:
        # 사용자가 입력한 단어
        answerword = search.split('정답')[1][1:]

        if db_master.select_quiz(user) != None:
            if answerword == db_master.select_quiz(user)[1]:
                outprint = "정답입니다!"

                # 단어장에서 데이터 삭제 메소드 호출
                db_master.delete((user, answerword))
                # 퀴즈 단어 삭제
                db_master.delete_quiz(user)
            else:
                outprint = "오답입니다!"
        else:
            return "'문제'를 먼저 입력해주세요. "
        return outprint

    elif "가이드" in search:
        outprint = '=== 명령어 예 ===\n'
        outprint += '@dic 검색 sky\n'
        outprint += '단어의 뜻을 검색하고 해당 단어가 본인의 단어장에 입력됩니다.\n'
        outprint += '@dic 단어장\n'
        outprint += '본인이 검색한 단어들로 구성된 단어리스트를 출력합니다.\n'
        outprint += '@dic 문제\n'
        outprint += '단어리스트 중 랜덤으로 문제를 출제합니다.\n'
        outprint += '@dic 정답 sky\n'
        outprint += '제시된 문제를 맞출 때 사용합니다.\n'

        return outprint
    else:
        outprint = '존재하지 않는 명령어입니다.\n'
        outprint += '=== 명령어 예 ===\n'
        outprint += '@dic 검색 sky\n'
        outprint += '단어의 뜻을 검색하고 해당 단어가 본인의 단어장에 입력됩니다.\n'
        outprint += '@dic 단어장\n'
        outprint += '본인이 검색한 단어들로 구성된 단어리스트를 출력합니다.\n'
        outprint += '@dic 문제\n'
        outprint += '단어리스트 중 랜덤으로 문제를 출제합니다.\n'
        outprint += '@dic 정답 sky\n'
        outprint += '제시된 문제를 맞출 때 사용합니다.\n'

        return outprint
    # 각 섹션을 list로 묶어 전달합니다
    return outprint

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    user = event_data["event"]["user"]

    message = crawl_dictionary(text, user)
    head_section = SectionBlock(
        text=message
    )

    button_actions = ActionsBlock(
        block_id=user,
        elements=[
            ButtonElement(
                text="단어장", style="danger",
                action_id="vocabulary", value=str("단어장")
            ),

            ButtonElement(
                text="문제",
                action_id="quiz", value=str("문제")
            ),

            ButtonElement(
                text="가이드",
                action_id="guide", value=str("가이드")
            ),
        ]
    )

    slack_web_client.chat_postMessage(
        channel=channel,
        text=message,
        blocks = extract_json([head_section,button_actions])
    )

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))
    keyword =  str(click_event.value)

    userdata= click_event.block_id
    button_actions = ActionsBlock(
        block_id=userdata,
        elements=[
            ButtonElement(
                text="단어장", style="danger",
                action_id="vocabulary", value=str("단어장")
            ),

            ButtonElement(
                text="문제",
                action_id="quiz", value=str("문제")
            ),
            ButtonElement(
                text="가이드",
                action_id="guide", value=str("가이드")
            ),
        ]
    )
    message_blocks = crawl_dictionary(keyword, userdata)
    head_section = SectionBlock(
        text=message_blocks,
    )
    # 메시지를 채널에 올립니다

    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        text=message_blocks,
        blocks=extract_json([head_section,button_actions])
    )
    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])

def index():
    return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)